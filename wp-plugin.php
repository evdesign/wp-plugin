<?php
/*
* Plugin Name: WP Plugin
* Plugin URI: https://evdesign.ru/
* Description: WP Plugin Boilerplate
* Version: 0.001
* Author: evdesign
* Author URI: https://evdesign.ru/
* License: GPLv2 or later
* Text Domain: wp-plugin
*/

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}
   
define('WP_PLUGIN__DIR', untrailingslashit(dirname(__FILE__)));
define('WP_PLUGIN__URL', plugin_dir_url(__FILE__));

class WP_Plugin
{
    public $plugin_domain;
    public $plugin_lower_domain;
    public $version;
    private static $instance = null;

    public function __construct()
    {
        $this->plugin_domain = 'wp-plugin';
        $this->version = '0.01';
        $this->plugin_lower_domain = str_replace('-', '_', $this->plugin_domain);
        // $this->views_dir = trailingslashit( dirname( __FILE__ ) ) . 'server/views';

        $this->require();
        $this->load();

        add_action('admin_enqueue_scripts', [$this, 'admin_enqueue_scripts']);
        add_action('wp_enqueue_scripts', [$this, 'wp_enqueue_scripts']);
    }

    private function require()
    {
        require_once __DIR__ . '/inc/helpers.php';
        require_once __DIR__ . '/inc/class-shortcodes.php';
    }

    private function load()
    {
        Shortcodes::getInstance();
    }

    public function admin_enqueue_scripts()
    {
        wp_enqueue_style($this->plugin_domain . '-styles', plugin_dir_url(__FILE__) . 'assets/admin/' . $this->plugin_domain . '-admin.min.css', [], $this->version);
        wp_enqueue_script($this->plugin_domain . '-scripts', plugin_dir_url(__FILE__) . 'assets/admin/' . $this->plugin_domain . '-admin.min.js', ['jquery'], $this->version, true);

        wp_localize_script($this->plugin_domain . '-scripts', $this->plugin_lower_domain . '_ajax', [
            'url' => admin_url('admin-ajax.php'),
            'nonce' => wp_create_nonce('myajax-nonce')
        ]);
    }

    public function wp_enqueue_scripts()
    {
        wp_enqueue_style($this->plugin_domain . '-styles', plugin_dir_url(__FILE__) . 'assets/front/' . $this->plugin_domain . '-front.min.css', [], $this->version);
        wp_enqueue_script($this->plugin_domain . '-scripts', plugin_dir_url(__FILE__) . 'assets/front/' . $this->plugin_domain . '-front.min.js', ['jquery'], $this->version, true);
    }

    /**
     * Locate template.
     *
     * Locate the called template.
     * Search Order:
     * 1. /themes/theme/wp-plugin/$template_name
     * 2. /themes/theme/$template_name
     * 3. /plugins/wp-plugin/views/$template_name.
     *
     * @since 1.0.0
     *
     * @param 	string 	$template_name			Template to load.
     * @param 	string 	$string $template_path	Path to templates.
     * @param 	string	$default_path			Default path to template files.
     * @return 	string 							Path to the template file.
     */
    public static function plugin_locate_template($template_name, $template_path = '', $default_path = '')
    {

        // Set variable to search in woocommerce-plugin-templates folder of theme.
        if (!$template_path) :
            $template_path = 'wp-plugin/views/';
        endif;

        // Set default plugin templates path.
        if (!$default_path) :
            $default_path = __DIR__ . '/views/'; // Path to the template folder
        endif;

        // Search template file in theme folder.
        $template = locate_template([
            $template_path . $template_name,
            $template_name
        ]);

        // Get plugins template file.
        if (!$template) :
            $template = $default_path . $template_name;
        endif;

        return apply_filters('plugin_locate_template', $template, $template_name, $template_path, $default_path);
    }

    /**
     * Get template.
     *
     * Search for the template and include the file.
     *
     * @since 1.0.0
     *
     * @see plugin_locate_template()
     *
     * @param string 	$template_name			Template to load.
     * @param array 	$args					Args passed for the template file.
     * @param string 	$string $template_path	Path to templates.
     * @param string	$default_path			Default path to template files.
     */
    public static function get_template($template_name, $args = array(), $tempate_path = '', $default_path = '')
    {

        if (is_array($args) && isset($args)) :
            extract($args);
        endif;

        $template_file = WP_Plugin::plugin_locate_template($template_name, $tempate_path, $default_path);

        if (!file_exists($template_file)) :
            _doing_it_wrong(__FUNCTION__, sprintf('<code>%s</code> does not exist.', $template_file), '1.0.0');
            return;
        endif;

        include $template_file;
    }


    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new WP_Plugin();
        }

        return self::$instance;
    }
}

WP_Plugin::getInstance();

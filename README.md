# README #

### What is this repository for? ###

* WP Plugin Boilerplate with webpack integration and live reload
* Version: 0.01

### How do I get set up? ###

1. git clone -> npm i 
2. Set config.json proxyURL variable
3. Update variables here according to plugin name wp-plugin.php
4. Set plugin name here package.json
5. Rename plugin folder to yours

Dev mode: npm run 
Build: npm run build

Output after build: zip file in plugin root

### Who do I talk to? ###

* Author: https://evdesign.ru/
<?php

class Shortcodes
{
    private static $instance = null;

    public function __construct()
    {
        $shortcodes = [
            'example',
        ];
        foreach ($shortcodes as $name) add_shortcode($name, [$this, $name . '_shortcode']);
    }

    public static function getInstance()
    {
        if (self::$instance == null) {
            self::$instance = new Shortcodes();
        }

        return self::$instance;
    }

    public function example_shortcode($atts, $content = null)
    {
        extract(shortcode_atts(array(
            'att' => '',
        ), $atts));

        return '<h1>Example</h1>';
    }
}
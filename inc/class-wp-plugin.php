<?php
class WP_Plugin_Init
{
    private static $instance = null;

    public function __construct()
    {
        $this->register_menu();

        // add_action('wp_ajax_php_run_once_ajax', [$this, 'php_run_once_ajax']);
        // add_action('wp_ajax_nopriv_php_run_once_ajax', [$this, 'php_run_once_ajax']);
    }

    private function register_menu()
    {
        add_action('admin_menu', function () {
            add_submenu_page('tools.php', 'PHP Run Once', 'PHP Run Once', 'administrator', 'php-run-once', [$this, 'admin_tab_view'], 8);
        });
    }


    public function php_run_once_ajax()
    {
        check_ajax_referer('myajax-nonce', 'nonce_code');

        if (!current_user_can('administrator')) wp_die();


        wp_die();
    }

    // only if the class has no instance.
    public static function getInstance()
    {
        dbg('getInstance');
        
        if (self::$instance == null) {
            self::$instance = new WP_Plugin_Init();
        }

        return self::$instance;
    }
}